// modular-related content

// Create a menu, add links to other pages
modular.addMenu('main');
modular.addMenuItem('main', 'Main', 'tileedit');
modular.addMenuItem('main', 'About', 'about');
modular.addMenuItem('main', 'Contact', 'contact');

for (let page of ['tileedit', 'about', 'contact']) {
  modular.runFuncOnload(page, () => modular.log('joining ' + page));
  modular.runFuncOffload(page, () => modular.log('leaving ' + page));
}

// todo move loadMaps button from index.html to one of the title bar buttons.
//modular.addMenuItem('main', 'Load Map', 'loadmap');
// modular.runFuncOnload('loadmap', () => {
//   modular.setPage('tileedit');
// });

modular.runJsOnloadOnce('tileedit', 'tileeditnav');

// Load first/landing page
modular.setPage('tileedit');

//modular.addMenu('test2')
//modular.addMenuItem('test2', 'New Jersey', 'maybe')
//modular.addMenuItem('test2', 'Farmhouse Ale', 'so')
//modular.setActiveMenuNavbar('test2')
//modular.setActiveMenuNavbar('test')

// modular.runJsOnloadOnce('page2', function() {
//   document.getElementById('addTest').addEventListener('click', () => {
//     modular.addMenuItem('test', 'stringOnButton', 'boobs');
//   });
// });

// modular.runJsOnload('page1', modular.getJs('page1'));

// modular.runJsOnloadOnce(bodyContentId, function)
