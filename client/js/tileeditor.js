var maps;
var mapData = {};

var tilesheets; // tilesheets['myTilesheet'].activeTile
var activeTilesheet;

function addMapLoadButton(map) {
  var btn = document.createElement('a');
  btn.classList.add('btn', 'col', 's3', 'truncate', 'modal-close');
  btn.innerHTML = 'Load ' + map;
  btn.addEventListener('click', e => {
    console.log('Requesting map ' + map);
    requestMap(map);
  });
  document.getElementById('loadMaps').append(btn);
}

// Initialization of socket stuffs.
if (socket) {
  console.log('Initializing tile editor socket connection');

  socket.on('maplist', list => {
    console.log('Received list of maps from server. Maps: ' + JSON.stringify(maps));
    if (!maps) {
      maps = list;
      for (let map of Object.keys(maps)) {
        addMapLoadButton(map);
      }
    } else { // todo merge two lists, check for deletions, etc
      for (let map of Object.keys(list)) {
        if (!maps[map]) {
          maps.push(list[map]);
          addMapLoadButton(map);
        } else {
          // todo compare values from our current list to update values
          // for now let's just replace the stored value
          maps[map] = list[map];
        }
      }
    }
  });

  socket.on('mapTile', tile => {
    mapData[tile.map][tile.y][tile.x] = tile.tile
    // todo update map display
    displayMap(tile.map);
  });

  // todo figure out why the cell transparency is a thing
  var drawCells = (canvas, img) => {
    // Reset canvas position (todo: if necessary)
    canvas.height = img.height;
    canvas.width = img.width;
    canvas.style.left = img.offsetLeft + 'px';
    canvas.style.top = img.offsetTop + 'px';
    // Grab the drawing context
    var context = canvas.getContext('2d');
    // wipe the slate clean
    context.clearRect(0, 0, canvas.width, canvas.height);
    // draw selected cell
    if (canvas.selectedX >= 0) {
      context.strokeStyle = '#FF00FF';
      context.rect(canvas.selectedX*32, canvas.selectedY*32, 32, 32);
      context.stroke();
    }
    // draw hover cell
    if (canvas.hoverX >= 0) {
      context.strokeStyle = '#000000';
      context.rect(canvas.hoverX*32, canvas.hoverY*32, 32, 32);
      context.stroke();
    }
  };
  socket.on('tilesheetImage', tilesheet => {
    if (tilesheet.error) {
      console.log('Cannot receive tilesheet: ' + tilesheet.error);
      return;
    }
    console.log('Received tilesheet: ' + tilesheet.id);
    var img = new Image();
    img.src = 'data:image/png;base64,' + tilesheet.image;
    document.getElementById('tilesheets').append(img);

    var canvas = document.createElement('canvas');
    canvas.style.position = 'absolute';
    canvas.height = img.height;
    canvas.width = img.width;
    canvas.style.left = img.offsetLeft + 'px';
    canvas.style.top = img.offsetTop + 'px';
    canvas.style['pointer-events'] = 'none'; // Allow pass-through events to the image
    document.getElementById('editor').append(canvas);

    // Add listeners for mouse events
    img.addEventListener('mousemove', ele => {
      // draw a rect over the mouse'd image
      canvas.hoverX = Math.floor((ele.pageX - img.offsetLeft) / 32);
      canvas.hoverY = Math.floor((ele.pageY - img.offsetTop) / 32);
      drawCells(canvas, img);
    });
    img.addEventListener('mouseout', ele => {
      canvas.hoverX = undefined;
      drawCells(canvas, img);
    });
    img.addEventListener('mouseup', ele => {
      canvas.selectedX = Math.floor((ele.pageX - img.offsetLeft) / 32);
      canvas.selectedY = Math.floor((ele.pageY - img.offsetTop) / 32);
      drawCells(canvas, img);
    });
  });
} else {
  console.log('Trying to init tileeditor code, but socket is uninitialized...');
  // would consider refreshing if this happens, but that may end up in perpetual loop.
  // maybe setTimeout and just try to recall this... non-function!?
}

// todos
// tileEdit
// -- made local changes, send to server. Mark tile as edited
// -- when server sends tile edit back, mark as complete (assuming it matches what we changed, otherwise multiple people are.. yeah.


function requestMap(map) {
  if (!socket) return;
  // todo Check to see if we already have the map open

  console.log('Requesting ' + map);

  // Preemptively build the data structure for the map tiles
  mapData[map] = new Array(maps[map].height);
  for (let y = 0; y < maps[map].height; y++) {
    mapData[map][y] = new Array(maps[map].width);
  }

  // Request the map
  socket.emit('requestMap', map);

  // todo Request tilesheets if we don't yet have them loaded in.
  for (let sheet of maps[map].tilesheets) {
    socket.emit('requestTilesheet', sheet);
  }
  // todo Create tab
  // todo Initialize map editor
  // todo unloaded tiles show up with placeholder icons, cannot be edited
  // todo renderMap - specify a div, create a map
}

function displayMap(map) {
  let display = document.getElementById(map + 'canvas');
  if (!display) { // if it doesn't exist, create it
    display = document.createElement('canvas');
    display.id = map + 'canvas';
    // Set some attributes
    display.width = maps[map].width * maps[map].tilesize;
    display.height = maps[map].height * maps[map].tilesize;
    // Append it to... something.
    //document.getElementById('todo').append(document.createElement('br'));
    //document.getElementById('todo').append(document.createElement('br'));
    document.getElementById('editor').append(display);
    // Add some mouse event listeners (copied from elsewhere, maybe generalize)
    img.addEventListener('mousemove', ele => {
      // draw a rect over the mouse'd image
      display.hoverX = Math.floor((ele.pageX - img.offsetLeft) / 32);
      display.hoverY = Math.floor((ele.pageY - img.offsetTop) / 32);
      drawCells(display, img);
    });
    img.addEventListener('mouseout', ele => {
      display.hoverX = undefined;
      drawCells(display, img);
    });
    img.addEventListener('mouseup', ele => {
      display.selectedX = Math.floor((ele.pageX - img.offsetLeft) / 32);
      display.selectedY = Math.floor((ele.pageY - img.offsetTop) / 32);
      drawCells(display, img);
    });
  }
  // Update the display
  updateDisplay(map, display);

}

function updateDisplay(map, display) {
  let context = display.getContext('2d');
  for (let y = 0; y < maps[map].height; y++) {
    for (let x = 0; x < maps[map].width; x++) {
      if (mapData[map][y][x]) {
        context.fillStyle = '#'+mapData[map][y][x].color;
        context.fillRect(x*32,y*32,32,32)
      }
    }
  }

  // just for ease-of-use/testing purposes
  return display;
}

function chopSheet(tilesheet) {
  var tilesheets = {tilesheet: {tilesize: 32, width: 8, height: 12}}; // find actual home
  // pre-initialize the tilesheet arrays
  for (let y = 0; y < tilesheets[tilesheet].height; y++) {
    tilesheetData[tilesheet] = new Array(tilesheets[tilesheet].height);
    for (let x = 0; x < tilesheets[tilesheet].width; x++) {
      tilesheetData[tilesheet][y] = new Array(tilesheets[tilesheet].width);
    }
  }
  // chop up single-image tilesheet into subimages
  // https://gamedev.stackexchange.com/questions/25755/dividing-up-spritesheet-in-javascript
  for (let y = 0; y < tilesheets[tilesheet].height; y++) {
    for (let x = 0; x < tilesheets[tilesheet].width; x++) {
      let sprite = new Image();
      sprite.src = 'img/tilesheet.png'; // << todo
      var context = 'asdkfjwe;ofiajsdf';
      context.drawImage(sprite, 0, y*x, 32, 32, 0, 0, 32, 32);
    }
  }
}
