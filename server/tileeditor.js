var fs = require('fs');

// todo
// Do I cut up the tilesheet serverside or clientside?

var logger;
// Read in map descriptors from filesystem, placeholder content here
// tilesize && incompatible tilesheets?
var maps = {
  'sample': {id: 0, desc: 'sample map', width: 12, height: 12, tilesize: 32, tilesheets: ['sample']},
  'wide': {id: 1, desc: 'wide sample map', width: 21, height: 12, tilesize: 32, tilesheets: ['sample']},
  'tall': {id: 2, desc: 'tall sample map', width: 12, height: 21, tilesize: 32, tilesheets: ['sample']},
  'multiSheets': {id: 3, desc: 'another map', width: 9, height: 9, tilesize: 32, tilesheets: ['sample', 'collision']}
};
var tilesheets = {
  'sample': {size: 32, width: 8, height: 15, path: 'tilesheets/sample.png'},
  'bonus': {size: 32, width: 8, height: 3, path: 'tilesheets/bonus.png'},
}
// the actual tilemaps.   tilemapid: [2d array of tile ids]
var mapTiles = {};

// generate a random map for sample code
for (let map of Object.keys(maps)) {
  mapTiles[map] = new Array(maps[map].height);
  for (let y = 0; y < maps[map].height; y++) {
    mapTiles[map][y] = new Array(maps[map].width);
    for (let x = 0; x < maps[map].width; x++) {
      mapTiles[map][y][x] = randTile('sample');
    }
  }
}

// Example/random tile for testing/debug
function randTile(tilesheet) {
  var tile = {
    color: randomColorCode(), // background color if graphic cannot be found
    tilesheet: tilesheet, // tilesheet to pull the graphic from
  };
  if (tilesheets[tilesheet]) {
    tile.sheetx = Math.floor(Math.random()*tilesheets[tilesheet].width);
    tile.sheety = Math.floor(Math.random()*tilesheets[tilesheet].height);
  } else { // this shouldn't happen, but.. y'know
    tile.sheetx = 0;
    tile.sheety = 0;
  }
  return tile;
}

// todo forfun, change row->tile loop to something more interesting, e.g. circular spiral from center
function sendMap(map, socket) {
  // Iterate through mapTiles[map], sending each tile individually
  if (!mapTiles[map]) {
    logger.error('Client ' + socket.handshake.address + ' requested a map we do not have data for, ' + map);
    return; // todo new map stuffs
  }
  logger.debug('Sending tiles from ' + map + ' to client ' + socket.handshake.address);
  //for (var row of mapTiles[map]) {
  //for (var tile of row) {
  for (var y = 0; y < mapTiles[map].length; y++) {
    for (var x = 0; x < mapTiles[map][0].length; x++) {
      socket.emit('mapTile', {map: map, tile: mapTiles[map][y][x], x: x, y: y});
    }
  }
  logger.debug('Finished sending tiles from ' + map + ' to client ' + socket.handshake.address);
}

function onMapRequest(socket) {
  socket.on('requestMap', map => {
    sendMap(map, socket);
  });
}
function onMapListRequest(socket) {
  socket.on('getMapList', () => {
    logger.debug('client requested map list');
    if (!maps) {
      logger.error('Client requested maps, but we don\'t have any!');
      return;
    }
    socket.emit('maplist', maps);
  });
}

function onTileEdit(socket) {
  // write tile changes to disk (and memory)
  // send tileChange to all other clients
}

// testing/temp/debug/whatevs
function onRequestTilesheet(socket) {
  socket.on('requestTilesheet', tilesheet => {
    logger.info('Client requested ' + tilesheet + ' tilesheet');
    fs.readFile(__dirname + '/../tilesheets/' + tilesheet + '.png', (error, buffer) => {
      if (error) {
        logger.error(error);
        socket.emit('tilesheetImage', {id: tilesheet, error: 'Cannot open tilemap'});
        return;
      }
      socket.emit('tilesheetImage', {id: tilesheet, image: buffer.toString('base64')});
    });
  });
}

module.exports = config => {
  logger = config.logger;
  this.initConnection = socket => {
    logger.info('tileedit initialized for socket');
    onMapRequest(socket);
    onMapListRequest(socket);
    onTileEdit(socket);
    onRequestTilesheet(socket);

  };
  return this;
}


// other stuff
function randomColorCode() {
  return (Math.floor(Math.random() * 16777215).toString(16)).padStart(6, '0');
}
